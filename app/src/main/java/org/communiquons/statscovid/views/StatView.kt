package org.communiquons.statscovid.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import org.communiquons.statscovid.R
import kotlin.math.absoluteValue

class StatView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        val view = View.inflate(context, R.layout.view_stats, null)
        addView(view)
    }

    fun setLabel(label: String) {
        findViewById<TextView>(R.id.label).text = label
    }

    fun setVariation(variation: Int) {
        val variationStr = (if (variation >= 0) "+ " else "- ") + variation.absoluteValue

        findViewById<TextView>(R.id.variation).text = variationStr
    }

    fun setValue(value: Int) {
        findViewById<TextView>(R.id.value).text = value.toString()
    }

    fun reset() {
        findViewById<TextView>(R.id.value).text = "-"
        findViewById<TextView>(R.id.variation).text = "-"
    }
}
