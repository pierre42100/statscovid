package org.communiquons.statscovid.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.view_error.view.*
import org.communiquons.statscovid.R


class ErrorView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var tryAgainListener: (() -> Unit)? = null

    init {
        val view = View.inflate(context, R.layout.view_error, null)
        addView(view)

        tryAgainButton.setOnClickListener {
            tryAgainListener?.let { it1 -> it1() }
        }
    }


}