package org.communiquons.statscovid.helpers

import okhttp3.OkHttpClient
import okhttp3.Request
import org.communiquons.statscovid.TESTS_DATA_URL
import org.communiquons.statscovid.data.TestInfo
import org.communiquons.statscovid.data.TestsData

/**
 * Get test data
 *
 * Throws in case of failure
 */
fun getTestsData(): TestsData {
    val req = Request.Builder().url(TESTS_DATA_URL).build()

    val res = OkHttpClient().newCall(req).execute()

    if (!res.isSuccessful)
        throw RuntimeException("Failed to fetch information!")

    val reader = res.body!!.charStream().buffered()

    val data = TestsData()

    reader.forEachLine { line ->

        if (line.startsWith("dep;"))
            return@forEachLine

        val split = line.split(";")

        data.add(
            TestInfo(
                split[0],
                org.threeten.bp.LocalDate.parse(split[1]),
                split[2].toInt(),
                split[3].toInt(),
                split[4].toInt()
            )
        )
    }

    reader.close()

    return data
}