package org.communiquons.statscovid.helpers

import okhttp3.OkHttpClient
import okhttp3.Request
import org.communiquons.statscovid.HOSPITALISATION_DATA_URL
import org.communiquons.statscovid.data.HospitalisationData
import org.communiquons.statscovid.data.HospitalisationDataList
import org.threeten.bp.LocalDate

/**
 * Get hospitalisation data
 */
fun getHospitalisationData(): HospitalisationDataList {
    val req = Request.Builder().url(HOSPITALISATION_DATA_URL).build()

    val res = OkHttpClient().newCall(req).execute()

    if (!res.isSuccessful)
        throw RuntimeException("Failed to fetch information!")

    val reader = res.body!!.charStream().buffered()

    val data = HospitalisationDataList()

    reader.forEachLine { line ->

        if (line.startsWith("\"dep\";"))
            return@forEachLine

        val split = line.split(";")

        // 0 = Men + Women
        if (split[1] != "0")
            return@forEachLine

        data.add(
            HospitalisationData(
                split[0],
                LocalDate.parse(fixCSVDate(split[2])),
                split[3].toInt(),
                split[4].toInt(),
                split[5].toInt(),
                split[6].toInt()
            )
        )
    }

    reader.close()


    data.sort()
    return data
}


fun fixCSVDate(input: String): String {
    var res = input.replace("\"", "")

    if (res.contains("/")) {
        val split = res.split("/")
        res = "${split[2]}-${split[1]}-${split[0]}"
    }

    return res
}