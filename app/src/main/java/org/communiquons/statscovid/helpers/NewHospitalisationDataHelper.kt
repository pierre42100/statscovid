package org.communiquons.statscovid.helpers

import okhttp3.OkHttpClient
import okhttp3.Request
import org.communiquons.statscovid.NEW_HOSPITALISATION_DATA_URL
import org.communiquons.statscovid.data.NewHospitalisationData
import org.communiquons.statscovid.data.NewHospitalisationDataList
import org.threeten.bp.LocalDate

/**
 * Get new hospitalisation data
 */
fun getNewHospitalisationData(): NewHospitalisationDataList {
    val req = Request.Builder().url(NEW_HOSPITALISATION_DATA_URL).build()

    val res = OkHttpClient().newCall(req).execute()

    if (!res.isSuccessful)
        throw RuntimeException("Failed to fetch information!")

    val reader = res.body!!.charStream().buffered()

    val data = NewHospitalisationDataList()

    reader.forEachLine { line ->

        if (line.startsWith("dep;"))
            return@forEachLine

        val split = line.split(";")

        data.add(
            NewHospitalisationData(
                split[0],
                LocalDate.parse(split[1]),
                split[2].toInt(),
                split[3].toInt(),
                split[4].toInt(),
                split[5].toInt()
            )
        )
    }

    reader.close()

    data.sort()
    return data
}