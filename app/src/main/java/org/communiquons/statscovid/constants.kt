package org.communiquons.statscovid

/**
 * URL where tests results are available
 */
const val TESTS_DATA_URL =
    "http://www.data.gouv.fr/fr/datasets/r/406c6a23-e283-4300-9484-54e78c8ae675"

/**
 * URL where the number of people currently in hospital / reanimation is available
 */
const val HOSPITALISATION_DATA_URL =
    "http://www.data.gouv.fr/fr/datasets/r/63352e38-d353-4b54-bfd1-f1b3ee1cabd7"

/**
 * URL where new hospitalisation data is available
 */
const val NEW_HOSPITALISATION_DATA_URL =
    "http://www.data.gouv.fr/fr/datasets/r/6fadff46-9efd-4c53-942a-54aca783c30c"