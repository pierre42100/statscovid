package org.communiquons.statscovid.ui.dashboard

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.setPadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import kotlinx.android.synthetic.main.fragment_hospitalisations.*
import org.communiquons.statscovid.R
import org.communiquons.statscovid.views.ErrorView
import org.threeten.bp.LocalDate

enum class GraphType {
    HOSPITALISATIONS_CHART,
    NEW_HOSPITALISATIONS_CHART,
    NEW_REANIMATION_CHART,
    NEW_DEATHS_CHART;
}

class HospitalisationsFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private lateinit var dashboardViewModel: DashboardViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProvider(this).get(DashboardViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_hospitalisations, container, false)

        val dataContainer: ConstraintLayout = root.findViewById(R.id.dataContainer)
        val errorView: ErrorView = root.findViewById(R.id.errorView)

        errorView.tryAgainListener = {
            dashboardViewModel.refreshData()
        }

        dashboardViewModel.data.observe(viewLifecycleOwner, Observer {
            when (it) {
                DataFetchStatus.Loading -> {
                    progressBar.visibility = View.VISIBLE
                    errorView.visibility = View.GONE
                    dataContainer.visibility = View.GONE
                }

                DataFetchStatus.Error -> {
                    progressBar.visibility = View.GONE
                    errorView.visibility = View.VISIBLE
                    dataContainer.visibility = View.GONE
                }

                is DataFetchStatus.Success -> {
                    progressBar.visibility = View.GONE
                    errorView.visibility = View.GONE
                    dataContainer.visibility = View.VISIBLE


                    applyData()
                }
            }
        })
        return root
    }

    private fun applyData() {
        chartSelector.onItemSelectedListener = this
        chartSelector.adapter = ChartSelectorAdapter(requireContext())

        chart.description.isEnabled = false
        chart.setDrawBorders(false)

        chart.axisRight.isEnabled = false
        chart.axisLeft.apply {
            setDrawAxisLine(false)
            setDrawGridLines(false)
            textColor = Color.WHITE
        }

        chart.xAxis.apply {
            setDrawAxisLine(false)
            setDrawGridLines(false)
            textColor = Color.WHITE

            valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    val date = LocalDate.ofEpochDay(value.toLong())
                    return "${date.dayOfMonth}/${date.monthValue}/${date.year}"
                }
            }
        }

        chart.setTouchEnabled(true)

        chart.legend.apply {
            verticalAlignment = Legend.LegendVerticalAlignment.TOP
            horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
            orientation = Legend.LegendOrientation.HORIZONTAL
            textColor = Color.WHITE
            setDrawInside(false)
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        // NOTHING YET
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, pos: Int, id: Long) {
        val chartType = (chartSelector.adapter as ChartSelectorAdapter).getItem(pos)

        chart.resetTracking()


        val dataSets = ArrayList<ILineDataSet>()

        when (chartType) {
            GraphType.HOSPITALISATIONS_CHART -> dataSets.addAll(getHospitalisationDataSet())
            GraphType.NEW_HOSPITALISATIONS_CHART -> dataSets.add(getNewHospitalisationDataSet())
            GraphType.NEW_REANIMATION_CHART -> dataSets.add(getNewReanimationDataSet())
            GraphType.NEW_DEATHS_CHART -> dataSets.add(addNewDeathsDataSet())
        }


        val data = LineData(dataSets)
        chart.data = data
        chart.fitScreen()
        chart.invalidate()
    }

    /**
     * Get hospitalisation dataSet
     */
    private fun getHospitalisationDataSet(): List<ILineDataSet> {
        val stats = dashboardViewModel.hospitalisationData.getStatsOfAllDays()
        val dataSets = ArrayList<ILineDataSet>()


        // Hospitalisations
        val valuesHosp = stats.map {
            Entry(
                it.day.toEpochDay().toFloat(),
                it.hospitalizations.toFloat()
            )
        }

        val dataSetHosp = LineDataSet(valuesHosp, "Hospitalisations")
        dataSetHosp.color = Color.YELLOW
        dataSetHosp.setDrawCircles(false)
        dataSets.add(dataSetHosp)


        // Reanimation
        val valuesRea =
            stats.map { Entry(it.day.toEpochDay().toFloat(), it.reanimation.toFloat()) }

        val dataSetRea = LineDataSet(valuesRea, "Réanimations")
        dataSetRea.color = Color.BLUE
        dataSetRea.setDrawCircles(false)
        dataSets.add(dataSetRea)


        // Deaths
        val valuesDeaths =
            stats.map { Entry(it.day.toEpochDay().toFloat(), it.death.toFloat()) }

        val dataSetDeath = LineDataSet(valuesDeaths, "Décès")
        dataSetDeath.color = Color.RED
        dataSetDeath.setDrawCircles(false)
        dataSets.add(dataSetDeath)

        // ReturnToHome
        val valuesReturnBackHome =
            stats.map { Entry(it.day.toEpochDay().toFloat(), it.returnToHome.toFloat()) }

        val dataSetReturnToHome = LineDataSet(valuesReturnBackHome, "Retour à domicile")
        dataSetReturnToHome.color = Color.GREEN
        dataSetReturnToHome.setDrawCircles(false)
        dataSets.add(dataSetReturnToHome)


        return dataSets
    }

    private fun getNewHospitalisationDataSet(): ILineDataSet {
        val stats = dashboardViewModel.newHospitalisationData.getStatsOfAllDays()

        val values = stats.map {
            Entry(
                it.day.toEpochDay().toFloat(),
                it.newHospitalisations.toFloat()
            )
        }

        val dataSet = LineDataSet(values, "Nouvelles hospitalisations")
        dataSet.color = Color.YELLOW
        dataSet.setDrawCircles(false)

        return dataSet
    }

    private fun getNewReanimationDataSet(): ILineDataSet {
        val stats = dashboardViewModel.newHospitalisationData.getStatsOfAllDays()

        val values = stats.map {
            Entry(
                it.day.toEpochDay().toFloat(),
                it.newReanimation.toFloat()
            )
        }

        val dataSet = LineDataSet(values, "Nouvelles réanimations")
        dataSet.color = Color.RED
        dataSet.setDrawCircles(false)

        return dataSet
    }

    private fun addNewDeathsDataSet(): ILineDataSet {
        val stats = dashboardViewModel.newHospitalisationData.getStatsOfAllDays()

        val values = stats.map {
            Entry(
                it.day.toEpochDay().toFloat(),
                it.newDeath.toFloat()
            )
        }

        val dataSet = LineDataSet(values, "Nouveaux décès")
        dataSet.color = Color.RED
        dataSet.setDrawCircles(false)

        return dataSet
    }
}

private class ChartSelectorAdapter(context: Context) :
    ArrayAdapter<GraphType>(context, android.R.layout.simple_spinner_item) {

    init {
        setDropDownViewResource(android.R.layout.simple_spinner_item)
    }

    override fun getCount(): Int {
        return GraphType.values().size
    }

    override fun getItem(position: Int): GraphType {
        return GraphType.values()[position]
    }

    fun getItemLabel(position: Int): String {
        val id = when (getItem(position)) {
            GraphType.HOSPITALISATIONS_CHART -> R.string.chart_hospitalisations
            GraphType.NEW_HOSPITALISATIONS_CHART -> R.string.chart_new_hospitalisations
            GraphType.NEW_REANIMATION_CHART -> R.string.chart_new_reanimation
            GraphType.NEW_DEATHS_CHART -> R.string.chart_new_deaths
        }

        return context.getString(id)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getView(position, convertView, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (view == null) {
            view =
                LayoutInflater.from(context)
                    .inflate(android.R.layout.simple_spinner_item, parent, false)
        }


        view!!.findViewById<TextView>(android.R.id.text1).text = getItemLabel(position)
        view.setPadding(15)

        return view
    }
}