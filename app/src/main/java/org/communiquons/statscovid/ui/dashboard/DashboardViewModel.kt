package org.communiquons.statscovid.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.communiquons.statscovid.data.HospitalisationDataList
import org.communiquons.statscovid.data.NewHospitalisationDataList
import org.communiquons.statscovid.helpers.getHospitalisationData
import org.communiquons.statscovid.helpers.getNewHospitalisationData

class DashboardViewModel : ViewModel() {

    private val _data =
        MutableLiveData<DataFetchStatus>().apply {
            refreshData()
        }


    val data: LiveData<DataFetchStatus> = _data


    fun refreshData() {
        Thread {
            _data.postValue(DataFetchStatus.Loading)

            try {
                _data.postValue(
                    DataFetchStatus.Success(getHospitalisationData(), getNewHospitalisationData())
                )
            } catch (e: Exception) {
                e.printStackTrace(System.err)
                _data.postValue(DataFetchStatus.Error)
            }

        }.start()
    }

    val hospitalisationData: HospitalisationDataList
        get() = when (_data.value) {
            is DataFetchStatus.Success -> (_data.value as DataFetchStatus.Success).hospData
            else -> throw RuntimeException("Trying to access non ready data!")
        }

    val newHospitalisationData: NewHospitalisationDataList
        get() = when (_data.value) {
            is DataFetchStatus.Success -> (_data.value as DataFetchStatus.Success).newHostData
            else -> throw RuntimeException("Trying to access non ready data!")
        }
}

sealed class DataFetchStatus {
    object Loading : DataFetchStatus()
    object Error : DataFetchStatus()
    data class Success(
        val hospData: HospitalisationDataList,
        val newHostData: NewHospitalisationDataList
    ) :
        DataFetchStatus()
}