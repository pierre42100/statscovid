package org.communiquons.statscovid.ui.tests

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.communiquons.statscovid.data.TestsData
import org.communiquons.statscovid.helpers.getTestsData
import java.lang.RuntimeException

class TestsViewModel : ViewModel() {

    private val _data = MutableLiveData<TestFetchStatus>().apply {
        refreshData()
    }


    val data: LiveData<TestFetchStatus> = _data


    fun refreshData() {
        Thread {
            _data.postValue(TestFetchStatus.Loading)

            try {
                _data.postValue(TestFetchStatus.Success(getTestsData()))
            } catch (e: Exception) {
                e.printStackTrace(System.err)
                _data.postValue(TestFetchStatus.Error)
            }

        }.start()
    }

    val testData : TestsData get() = when(_data.value) {
        is TestFetchStatus.Success -> (_data.value as TestFetchStatus.Success).d
        else -> throw RuntimeException("Trying to access non ready data!")
    };
}

sealed class TestFetchStatus {
    object Loading : TestFetchStatus()
    object Error : TestFetchStatus()
    data class Success(val d: TestsData) : TestFetchStatus()
}