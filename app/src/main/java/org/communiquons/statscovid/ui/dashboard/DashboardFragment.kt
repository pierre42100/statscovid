package org.communiquons.statscovid.ui.dashboard

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.communiquons.statscovid.R
import org.communiquons.statscovid.views.ErrorView
import org.threeten.bp.LocalDate

class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    private var currDay: LocalDate = LocalDate.now()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProvider(this).get(DashboardViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)

        val dataContainer: ConstraintLayout = root.findViewById(R.id.dataContainer)
        val errorView: ErrorView = root.findViewById(R.id.errorView)

        errorView.tryAgainListener = {
            dashboardViewModel.refreshData()
        }

        dashboardViewModel.data.observe(viewLifecycleOwner, Observer {
            when (it) {
                DataFetchStatus.Loading -> {
                    progressBar.visibility = View.VISIBLE
                    errorView.visibility = View.GONE
                    dataContainer.visibility = View.GONE
                }

                DataFetchStatus.Error -> {
                    progressBar.visibility = View.GONE
                    errorView.visibility = View.VISIBLE
                    dataContainer.visibility = View.GONE
                }

                is DataFetchStatus.Success -> {
                    progressBar.visibility = View.GONE
                    errorView.visibility = View.GONE
                    dataContainer.visibility = View.VISIBLE

                    currDay = dashboardViewModel.hospitalisationData.last().day
                    applyData()
                }
            }
        })
        return root
    }


    @SuppressLint("SetTextI18n")
    fun applyData() {

        // Day selector
        dateView.text = "${currDay.dayOfMonth}/${currDay.monthValue}/${currDay.year}"

        previousDayButton.setOnClickListener {
            currDay = currDay.minusDays(1)
            applyData()
        }

        nextDayButton.setOnClickListener {
            currDay = currDay.plusDays(1)
            applyData()
        }

        nextDayButton.visibility = if (currDay == LocalDate.now()) View.INVISIBLE else View.VISIBLE

        val twoPrevDay = currDay.minusDays(2)
        previousDayButton.visibility =
            if (dashboardViewModel.hospitalisationData[0].day.isAfter(twoPrevDay) ||
                dashboardViewModel.newHospitalisationData[0].day.isAfter(twoPrevDay)
            ) View.GONE else View.VISIBLE


        // Collect data
        val prevHosp = dashboardViewModel.hospitalisationData.getStatsOfDay(currDay.minusDays(1))
        val currHosp = dashboardViewModel.hospitalisationData.getStatsOfDay(currDay)

        val prevNewHosp =
            dashboardViewModel.newHospitalisationData.getStatsOfDay(currDay.minusDays(1))
        val currNewHosp = dashboardViewModel.newHospitalisationData.getStatsOfDay(currDay)


        if (prevHosp == null || currHosp == null || prevNewHosp == null || currNewHosp == null) {
            hospitalisations.reset()
            new_hospitalisations.reset()
            reanimation.reset()
            new_reanimation.reset()
            deaths.reset()
            new_deaths.reset()
            return_to_home.reset()
            new_return_to_home.reset()
            return
        }

        hospitalisations.setLabel(getString(R.string.hospitalisations_label))
        hospitalisations.setValue(currHosp.hospitalizations)
        hospitalisations.setVariation(currHosp.hospitalizations - prevHosp.hospitalizations)

        new_hospitalisations.setLabel(getString(R.string.new_hospitalisations_label))
        new_hospitalisations.setValue(currNewHosp.newHospitalisations)
        new_hospitalisations.setVariation(currNewHosp.newHospitalisations - prevNewHosp.newHospitalisations)

        reanimation.setLabel(getString(R.string.reanimations_label))
        reanimation.setValue(currHosp.reanimation)
        reanimation.setVariation(currHosp.reanimation - prevHosp.reanimation)

        new_reanimation.setLabel(getString(R.string.new_reanimations_label))
        new_reanimation.setValue(currNewHosp.newReanimation)
        new_reanimation.setVariation(currNewHosp.newReanimation - prevNewHosp.newReanimation)

        deaths.setLabel(getString(R.string.death_labels))
        deaths.setValue(currHosp.death)
        deaths.setVariation(currHosp.death - prevHosp.death)

        new_deaths.setLabel(getString(R.string.new_deaths_label))
        new_deaths.setValue(currNewHosp.newDeath)
        new_deaths.setVariation(currNewHosp.newDeath - prevNewHosp.newDeath)

        return_to_home.setLabel(getString(R.string.return_to_home_label))
        return_to_home.setValue(currHosp.returnToHome)
        return_to_home.setVariation(currHosp.returnToHome - prevHosp.returnToHome)

        new_return_to_home.setLabel(getString(R.string.new_return_to_home_label))
        new_return_to_home.setValue(currNewHosp.newReturnToHome)
        new_return_to_home.setVariation(currNewHosp.newReturnToHome - prevNewHosp.newReturnToHome)
    }
}