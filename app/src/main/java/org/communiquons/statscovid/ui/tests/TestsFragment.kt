package org.communiquons.statscovid.ui.tests

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.StackedValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import kotlinx.android.synthetic.main.fragment_tests.*
import org.communiquons.statscovid.R
import org.communiquons.statscovid.views.ErrorView


class TestsFragment : Fragment() {

    private lateinit var testsViewModel: TestsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        testsViewModel = ViewModelProvider(this).get(TestsViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_tests, container, false)

        root.findViewById<ErrorView>(R.id.errorView).tryAgainListener = {
            testsViewModel.refreshData()
        }

        testsViewModel.data.observe(viewLifecycleOwner, Observer {

            when (it) {
                TestFetchStatus.Loading -> {
                    progressBar.visibility = View.VISIBLE
                    errorView.visibility = View.GONE
                    chart.visibility = View.GONE
                }
                TestFetchStatus.Error -> {
                    progressBar.visibility = View.GONE
                    errorView.visibility = View.VISIBLE
                    chart.visibility = View.GONE
                }
                is TestFetchStatus.Success -> {
                    progressBar.visibility = View.GONE
                    errorView.visibility = View.GONE
                    chart.visibility = View.VISIBLE

                    checkBoxPositiveTests.setOnCheckedChangeListener { _, _ -> refreshChat() }

                    refreshChat()
                }
            }

        })

        return root
    }

    private fun refreshChat() {
        if (checkBoxPositiveTests.isChecked) {
            refreshChartShowPercentage()
        } else {
            refreshChartShowGeneralData()
        }
    }

    private fun refreshChartShowPercentage() {

        chart.resetZoom()
        chart.resetTracking()

        chart.legend.apply {
            isEnabled = true
            textColor = Color.WHITE
            verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
            horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
            orientation = Legend.LegendOrientation.HORIZONTAL
            setDrawInside(true)
            formSize = 8f
            formToTextSpace = 4f
            xEntrySpace = 6f
            form = Legend.LegendForm.SQUARE
            textSize = 15f
        }

        chart.xAxis.apply {
            textColor = Color.WHITE
            position = XAxis.XAxisPosition.TOP_INSIDE
        }


        chart.axisRight.isEnabled = false


        val generalData = testsViewModel.testData.getGeneral()
        val values = ArrayList<Entry>()


        var i = 0
        var maxValue = 0f
        generalData.forEach { entry ->

            val currPercentage = 100 * (entry.positive.toFloat() / entry.total.toFloat())

            if (currPercentage > maxValue)
                maxValue = currPercentage

            values.add(
                Entry(
                    i++.toFloat(),
                    currPercentage,
                    entry.day
                )
            )
        }

        chart.axisLeft.apply {
            textColor = Color.WHITE
            axisMaximum = maxValue + 5
            axisMinimum = 0f
        }

        chart.xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                val day = generalData[value.toInt()].day
                return "${day.dayOfMonth}/${day.monthValue}/${day.year}"
            }
        }

        val set1 = LineDataSet(values, "% tests positifs")
        set1.setDrawIcons(false)
        set1.setDrawCircles(false)

        val dataSets: ArrayList<ILineDataSet> = ArrayList()
        dataSets.add(set1)

        val lineData = LineData(dataSets).also {
            it.setValueFormatter(StackedValueFormatter(false, "", 1))
            it.setValueTextColor(Color.WHITE)
        }


        val cmbData = CombinedData()
        cmbData.setData(lineData)
        cmbData.setData(BarData(ArrayList()))
        chart.renderer = null
        chart.data = cmbData


        chart.fitScreen()
        chart.invalidate()

    }

    private fun refreshChartShowGeneralData() {

        chart.description.apply {
            isEnabled = true
            textColor = Color.WHITE
        }

        chart.setMaxVisibleValueCount(40)
        chart.setPinchZoom(true)

        chart.legend.apply {
            isEnabled = true
            textColor = Color.WHITE
            verticalAlignment = Legend.LegendVerticalAlignment.TOP
            horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
            orientation = Legend.LegendOrientation.HORIZONTAL
            setDrawInside(true)
            formSize = 8f
            formToTextSpace = 4f
            xEntrySpace = 6f
            form = Legend.LegendForm.SQUARE
            textSize = 15f

        }

        chart.xAxis.apply {
            textColor = Color.WHITE
        }

        chart.axisLeft.apply {
            textColor = Color.WHITE
            resetAxisMinimum()
            resetAxisMaximum()
        }

        chart.axisRight.isEnabled = false


        val generalData = testsViewModel.testData.getGeneral()
        val values = ArrayList<BarEntry>()


        var i = 0
        generalData.forEach { entry ->
            values.add(
                BarEntry(
                    i++.toFloat(),
                    floatArrayOf(
                        entry.positive.toFloat(),
                        (entry.total - entry.positive).toFloat()
                    ),
                    entry.day
                )
            )
        }

        chart.xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                val day = generalData[value.toInt()].day
                return "${day.dayOfMonth}/${day.monthValue}/${day.year}"
            }
        }

        val set1 = BarDataSet(values, "Tests")
        set1.setDrawIcons(false)
        set1.stackLabels = arrayOf("Positifs", "Négatifs")
        set1.colors = getColors().toList()


        val dataSets: ArrayList<IBarDataSet> = ArrayList()
        dataSets.add(set1)

        val data = BarData(dataSets)
        data.setValueFormatter(StackedValueFormatter(false, "", 1))
        data.setValueTextColor(Color.WHITE)

        chart.data = CombinedData().also { it.setData(data) }

        chart.fitScreen()
        chart.invalidate()

    }

    private fun getColors(): Array<Int> {
        return arrayOf(Color.RED, Color.GREEN)
    }
}