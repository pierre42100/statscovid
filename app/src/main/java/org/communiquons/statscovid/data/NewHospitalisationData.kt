package org.communiquons.statscovid.data

import org.threeten.bp.LocalDate

class NewHospitalisationData(
    val dep: String,
    val day: LocalDate,
    val newHospitalisations: Int,
    val newReanimation: Int,
    val newDeaths: Int,
    val newReturnToHome: Int
) : Comparable<NewHospitalisationData> {
    override fun compareTo(other: NewHospitalisationData): Int {
        return day.compareTo(other.day)
    }
}

class NewHospitalisationDayStats(
    val day: LocalDate,
    var newHospitalisations: Int,
    var newReanimation: Int,
    var newReturnToHome: Int,
    var newDeath: Int
)

class NewHospitalisationDataList : ArrayList<NewHospitalisationData>() {

    /**
     * Returns null if no data is available
     */
    fun getStatsOfDay(d: LocalDate): NewHospitalisationDayStats? {
        val stats = NewHospitalisationDayStats(d, 0, 0, 0, 0)
        var hasData = false

        forEach {
            if (it.day == d) {
                hasData = true
                stats.newHospitalisations += it.newHospitalisations
                stats.newReanimation += it.newReanimation
                stats.newReturnToHome += it.newReturnToHome
                stats.newDeath += it.newDeaths
            }
        }

        if (!hasData)
            return null

        return stats
    }

    /**
     * Export statistics of all days
     */
    fun getStatsOfAllDays(): ArrayList<NewHospitalisationDayStats> {
        val res = ArrayList<NewHospitalisationDayStats>()
        var stats = NewHospitalisationDayStats(get(0).day, 0, 0, 0, 0)

        sort()

        forEach {

            if (it.day.isBefore(stats.day))
                throw RuntimeException("Invalid sort!")

            if (it.day != stats.day) {
                res.add(stats)

                stats = NewHospitalisationDayStats(
                    it.day,
                    it.newHospitalisations,
                    it.newReanimation,
                    it.newReturnToHome,
                    it.newDeaths
                )

            } else {
                stats.newHospitalisations += it.newHospitalisations
                stats.newReanimation += it.newReanimation
                stats.newReturnToHome += it.newReturnToHome
                stats.newDeath += it.newDeaths
            }

        }

        res.add(stats)

        return res
    }
}