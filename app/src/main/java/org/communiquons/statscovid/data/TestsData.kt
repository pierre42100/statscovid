package org.communiquons.statscovid.data

import org.threeten.bp.LocalDate

class TestInfo(
    val dep: String,
    val day: LocalDate,
    val positive: Int,
    val total: Int,
    val ageClass: Int
) : Comparable<TestInfo> {
    override fun compareTo(other: TestInfo): Int {
        return this.day.compareTo(other.day)
    }

}

class GeneralTestResultEntry(
    val day: LocalDate,
    val positive: Int,
    val total: Int
) : Comparable<GeneralTestResultEntry> {
    override fun compareTo(other: GeneralTestResultEntry): Int {
        return this.day.compareTo(other.day)
    }
}

class TestsData : ArrayList<TestInfo>() {


    fun getGeneral(): ArrayList<GeneralTestResultEntry> {
        sort()

        val res = ArrayList<GeneralTestResultEntry>()

        var currDate = this[0].day;
        var currPositives = 0;
        var currTotal = 0;


        forEach {

            if (currDate != it.day) {
                res.add(GeneralTestResultEntry(currDate, currPositives, currTotal))

                currDate = it.day
                currPositives = it.positive
                currTotal = it.total
            }


            currPositives += it.positive;
            currTotal += it.total;
        }

        res.add(GeneralTestResultEntry(currDate, currPositives, currTotal))

        return res

    }


}