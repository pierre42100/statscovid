package org.communiquons.statscovid.data

import org.threeten.bp.LocalDate

class HospitalisationData(
    val dep: String,
    val day: LocalDate,
    val hospitalizations: Int,
    val reanimation: Int,
    val returnToHome: Int,
    val death: Int
) : Comparable<HospitalisationData> {

    override fun compareTo(other: HospitalisationData): Int {
        return day.compareTo(other.day)
    }
}

class HospitalisationDayStats(
    val day: LocalDate,
    var hospitalizations: Int,
    var reanimation: Int,
    var returnToHome: Int,
    var death: Int
)

class HospitalisationDataList : ArrayList<HospitalisationData>() {

    /**
     * Returns null if no data is available
     */
    fun getStatsOfDay(d: LocalDate): HospitalisationDayStats? {
        val stats = HospitalisationDayStats(d, 0, 0, 0, 0)
        var hasData = false

        forEach {
            if (it.day == d) {
                hasData = true
                stats.hospitalizations += it.hospitalizations
                stats.reanimation += it.reanimation
                stats.returnToHome += it.returnToHome
                stats.death += it.death
            }
        }

        if (!hasData)
            return null

        return stats
    }

    /**
     * Export statistics of all days
     */
    fun getStatsOfAllDays(): ArrayList<HospitalisationDayStats> {
        val res = ArrayList<HospitalisationDayStats>()
        var stats = HospitalisationDayStats(get(0).day, 0, 0, 0, 0)

        sort()

        forEach {

            if (it.day.isBefore(stats.day))
                throw RuntimeException("Invalid sort!")

            if (it.day != stats.day) {
                res.add(stats)

                stats = HospitalisationDayStats(
                    it.day,
                    it.hospitalizations,
                    it.reanimation,
                    it.returnToHome,
                    it.death
                )

            } else {
                stats.hospitalizations += it.hospitalizations
                stats.reanimation += it.reanimation
                stats.returnToHome += it.returnToHome
                stats.death += it.death
            }

        }

        res.add(stats)

        return res
    }
}